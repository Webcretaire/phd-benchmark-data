The input file used to configure HPL is obtained from HPL.dat.144.dist, by replacing
- **#%PROBLEM_SIZE%#** by the desired problem size
- **#%PGRID_P%#** by the number of machines used
- **#%PGRID_Q%#** by the number of processes per machine