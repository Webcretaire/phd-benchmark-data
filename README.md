This repository holds data and graphs associated with the PhD *A full stack
simulator for HPC: Multi-level modelling of the BXI interconnect to predict the
performance of MPI applications* ([*Un simulateur pour le calcul haute
performance : modélisation multi-niveau de l'interconnect BXI pour prédire les
performances d'applications MPI*](https://www.theses.fr/s238434) in French).

The experiments are divided in categories, which correspond to the chapter in
which the experiment appears in the thesis.

All real-world experiments were performed on a cluster with nodes equipped with
BXI v2 hardware, AMD EPYC™ 7763 64-Core processors, and 256GB of RAM.